#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

SoftwareSerial s(D2, D1); // setting serial pin rx, tx

ESP8266WiFiMulti WiFiMulti;

// silahkan diganti dulu sesuai dengan setting wifi dan lokal server
const char* ssid = "bersyukurlah"; // nama wifi
const char* password = "dariyanti"; // wifi password
String host = "192.168.43.9"; // ip dari lokal server
uint16_t port = 80; // port mu

unsigned long waktu_sekarang = 0; // waktu inisialisasi
void setup() {
  // set up
  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }
  // Initialize Serial port
  Serial.begin(115200);
  s.begin(115200);
  WiFi.mode(WIFI_OFF);        // Prevents reconnection issue (taking too long to connect)
  delay(1000);
  WiFi.mode(WIFI_STA);        // start wifi station
  WiFi.begin(ssid, password);     // Connect to your WiFi router
  Serial.println("");

  Serial.print("Connecting");
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("[WIFI] connected...\n");
  Serial.print("[WIFI] IP :");
  Serial.printf("%s\n", WiFi.localIP().toString().c_str());
  Serial.print("[WIFI] GATEWAY :");
  Serial.printf("%s\n", WiFi.gatewayIP().toString().c_str());
  Serial.print("[WIFI] DNS :");
  Serial.printf("%s\n", WiFi.dnsIP().toString().c_str());
  while (!Serial) continue;
  Serial.printf("[SETUP] DONE ...");

}

void loop() {
  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {
      waktu_sekarang = millis();
      Serial.print("waktu sekarang = ");
      Serial.println(waktu_sekarang / 1000);
      Serial.print("[SERIAL] get data ...\n");

      WiFiClient client; // persiapkan client wifi setting
      HTTPClient http; // persiapkan http

      // versi 5
      StaticJsonBuffer<1000> jsonBuffer; // persiapan buffer untuk menampung data JSON
      Serial.println(jsonBuffer.size());
      JsonObject& object = jsonBuffer.parseObject(s); // membuat instance variabel untuk menampung data JSON
      Serial.println(object.size());
      object.prettyPrintTo(Serial);
      if (object == JsonObject::invalid()) {
        Serial.print("[SERIAL] DATA INVALID...\n");
        return;
      }

      // versi 6
      //      const size_t capacity = JSON_OBJECT_SIZE(2) + 20;
      //      DynamicJsonDocument object(capacity);
      //      DeserializationError err = deserializeJson(object, s);
      //      if (err) {
      //        Serial.print(F("deserializeJson() failed: "));
      //        Serial.println(err.c_str());
      //      }
      //
      //      deserializeJson(object, s);
      
      // script diatas akan mendestrukturisasi object json
      // json awalnya 
      // {
      //    "detak" : "80",
      //    "suhu" : "29"
      // }
      
      int detak = object["detak"]; // ambil detak jantung
      float suhu = object["suhu"]; // ambil suhu
      
      Serial.print("[ALAT] Suhu Sekarang : ");
      Serial.print(suhu, 3);
      Serial.print("\n[ALAT] ♥");
      Serial.print(" BPM: ");
      Serial.print(detak);
      Serial.println();

      // buat string url dengan method GET
      // kenapa gak diganti menjadi POST ? karena gak tau gimana mekanismenya, kwkwk

      String url = "/pulse-temperature-web/public/Real/postData?detak="; // alamat dari codeigniter yang dipake
      url += detak;
      url += "&suhu=";
      url += suhu;

      // string url diatas akan menjadi semisal http://IP-DARI-LOKAL-SERVER:PORTNYA/pulse-temperature-web/public/Data/postData?detak=100&suhu=100
      Serial.print("[HTTP] begin...\n");
      if (http.begin(client, host, port, url)) {
        Serial.print("[HTTP] SEND ...\n");
        Serial.print("[HTTP] SEND TO ");
        Serial.println(url);
        // kita cek dahulu, apakah kita mendapatkan respons dari server
        int httpCode = http.GET();

        // http code kalau negatif akan error
        // referensi dari ESP8266 error code https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266HTTPClient/src/ESP8266HTTPClient.h#L49

        if (httpCode > 0) {
          // oke kita dapat HTTP code nya
          Serial.printf("[HTTP] SEND ... code: %d\n", httpCode);

          // Response status code dari server
          // referensi dari HTTP Response status code https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
          // referensi dari ESP8266 status code https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266HTTPClient/src/ESP8266HTTPClient.h#L67

          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            // dapatkan payloadnya, kita print kan
            String payload = http.getString();
            Serial.print("[HTTP] Status = ");
            Serial.println(payload);
          }

        } else {
          // kalau gak kekirim
          Serial.printf("[HTTP] SEND... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }
        http.end();
      } else {
        // kalau http nya gak konek, alias salah setting
        Serial.printf("[HTTP] Unable to connect\n");
      }
//    }

  } else {
    // kalau wifi gak konek
    Serial.print("[WIFI] not connected...\n");
  }
  delay(1000);

}
