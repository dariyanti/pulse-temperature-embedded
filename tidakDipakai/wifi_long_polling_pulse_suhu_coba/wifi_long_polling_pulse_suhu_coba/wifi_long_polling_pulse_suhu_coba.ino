// Board yang digunakan Generic ESP8266 Module yang biasa
// Sketch yang dipakai menggunakan push button
// VCC => 3.3V
// CH_PD // CH_EN => 3.3V
// GND => GND
// UTXD => TX
// URXD => RX
// GPIO0 => GND
// GPI02 => VCC
// untuk RST dan button ini masukkan RST ke salah satu kaki kaki dari BUTTON,
// kemudian dari kaki kaki yang satunya sambungkan ke GND
// RST => BUTTON
// BUTTON => GND

#define ONE_WIRE_BUS 2
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <Ticker.h>

ESP8266WiFiMulti WiFiMulti;

// setup sensor
OneWire oneWire(ONE_WIRE_BUS);

// berikan nama variabel,masukkan ke pustaka Dallas
DallasTemperature sensorSuhu(&oneWire);

const int PulseWire = 0;

// silahkan diganti dulu sesuai dengan setting wifi dan lokal server
const char* ssid = "bersyukurlah";
const char* password = "dariyanti";
String host = "192.168.43.208"; // ip dari lokal server
uint16_t port = 80;

Ticker flipper;

int fadeRate = 0;                 // used to fade LED on with PWM on fadePin
// these variables are volatile because they are used during the interrupt service routine!
volatile int BPM;                   // used to hold the pulse rate
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // holds the time between beats, must be seeded!
volatile boolean Pulse = false;     // true when pulse wave is high, false when it's low
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.
volatile int rate[10];                    // array to hold last ten IBI values
volatile unsigned long sampleCounter = 0; // used to determine pulse timing
volatile unsigned long lastBeatTime = 0;  // used to find IBI
volatile int P = 512;                     // used to find peak in pulse wave, seeded
volatile int T = 512;                     // used to find trough in pulse wave, seeded
volatile int thresh = 512;                // used to find instant moment of heart beat, seeded
volatile int amp = 100;                   // used to hold amplitude of pulse waveform, seeded
volatile boolean firstBeat = true;        // used to seed rate array so we startup with reasonable BPM
volatile boolean secondBeat = false;      // used to seed rate array so we startup with reasonable BPM

void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();
  Serial.println();
  Serial.println();
  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }
  // mode station
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);
  Serial.print("[SETUP] PULSE SENSOR ...\n");
  interruptSetup();
  Serial.println("[SETUP] PULSE READY !");
  Serial.print("[SETUP] SENSOR SUHU ...\n");
  sensorSuhu.begin();
  Serial.print("[SETUP] DONE SENSOR SUHU ...\n");
}

void loop() {
  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {
    // print aja semua info dari wifi yang terkoneksi
    Serial.print("[WIFI] connected...\n");
    Serial.print("[WIFI] IP :");
    Serial.printf("%s\n", WiFi.localIP().toString().c_str());
    Serial.print("[WIFI] GATEWAY :");
    Serial.printf("%s\n", WiFi.gatewayIP().toString().c_str());
    Serial.print("[WIFI] DNS :");
    Serial.printf("%s\n", WiFi.dnsIP().toString().c_str());
    WiFiClient client;
    HTTPClient http;
    Serial.print("[HTTP] begin...\n");
    int detakJantung = 0;
    float suhu = ambilSuhu();
    Serial.print("[ALAT] Suhu Sekarang : ");
    Serial.print(suhu, 3);
    if (QS == true) {                      // Quantified Self flag is true when arduino finds a heartbeat
      fadeRate = 255;                  // Set 'fadeRate' Variable to 255 to fade LED with pulse
      detakJantung = BPM;
      QS = false;                      // reset the Quantified Self flag for next time
    }
    Serial.print("\n[ALAT] ♥");
    Serial.print(" BPM: ");
    Serial.print(detakJantung);

    // buat string url dengan method GET
    // kenapa gak diganti menjadi POST ? karena gak tau gimana mekanismenya, kwkwk

    String url = "/pulse-temperature-web/public/Test/postData?detak="; // alamat dari codeigniter yang dipake
    url += detakJantung;
    url += "&suhu=";
    url += suhu;
    Serial.print("\n[HTTP] SENDING TO ");
    Serial.print("http://" + host + url);

    // string url diatas akan menjadi semisal http://IP-DARI-LOKAL-SERVER:PORTNYA/pulse-temperature-web/public/Data/postData?detak=100&suhu=100

    if (http.begin(client, host, port, url)) {
      Serial.print("\n[HTTP] SEND ...\n");

      // kita cek dahulu, apakah kita mendapatkan respons dari server
      int httpCode = http.GET();
      if (httpCode > 0) {
        // oke kita dapat HTTP code nya
        Serial.printf("[HTTP] SEND ... code: %d\n", httpCode);

        // Response status code dari server
        // referensi dari HTTP Response status code https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
        // referensi dari ESP8266 status code https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266HTTPClient/src/ESP8266HTTPClient.h#L67

        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          // dapatkan payloadnya, kita print kan
          String payload = http.getString();
          Serial.println(payload);
        }

      } else {
        // kalau gak kekirim
        Serial.printf("[HTTP] SEND... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }
      http.end();
    } else {
      // kalao http nya gak konek, alias salah setting
      Serial.printf("[HTTP] Unable to connect\n");
    }

  } else {
    // kalau wifi gak konek
    Serial.print("[WIFI] not connected...\n");
  }
  // delay 1 detik
  delay(1000);
}

float ambilSuhu() {
  sensorSuhu.requestTemperatures();
  float suhu = sensorSuhu.getTempCByIndex(0);
  return suhu;
}

void interruptSetup() {
  // Initializes Ticker to have flipper run the ISR to sample every 2mS as per original Sketch.
  flipper.attach_ms(2, ISRTr);
}


// THIS IS THE TICKER INTERRUPT SERVICE ROUTINE.
// Ticker makes sure that we take a reading every 2 miliseconds
void ISRTr() {                        // triggered when flipper fires....
  cli();                               // disable interrupts while we do this
  Signal = analogRead(PulseWire);              // read the Pulse Sensor
  sampleCounter += 2;                         // keep track of the time in mS with this variable
  int N = sampleCounter - lastBeatTime;       // monitor the time since the last beat to avoid noise

  //  find the peak and trough of the pulse wave
  if (Signal < thresh && N > (IBI / 5) * 3) { // avoid dichrotic noise by waiting 3/5 of last IBI
    if (Signal < T) {                       // T is the trough
      T = Signal;                         // keep track of lowest point in pulse wave
    }
  }

  if (Signal > thresh && Signal > P) {        // thresh condition helps avoid noise
    P = Signal;                             // P is the peak
  }                                        // keep track of highest point in pulse wave

  //  NOW IT'S TIME TO LOOK FOR THE HEART BEAT
  // signal surges up in value every time there is a pulse
  if (N > 250) {                                  // avoid high frequency noise
    if ( (Signal > thresh) && (Pulse == false) && (N > (IBI / 5) * 3) ) {
      Pulse = true;                               // set the Pulse flag when we think there is a pulse
      //      digitalWrite(blinkPin, HIGH);               // turn on pin 13 LED
      IBI = sampleCounter - lastBeatTime;         // measure time between beats in mS
      lastBeatTime = sampleCounter;               // keep track of time for next pulse

      if (secondBeat) {                      // if this is the second beat, if secondBeat == TRUE
        secondBeat = false;                  // clear secondBeat flag
        for (int i = 0; i <= 9; i++) {       // seed the running total to get a realisitic BPM at startup
          rate[i] = IBI;
        }
      }

      if (firstBeat) {                       // if it's the first time we found a beat, if firstBeat == TRUE
        firstBeat = false;                   // clear firstBeat flag
        secondBeat = true;                   // set the second beat flag
        sei();                               // enable interrupts again
        return;                              // IBI value is unreliable so discard it
      }


      // keep a running total of the last 10 IBI values
      word runningTotal = 0;                  // clear the runningTotal variable

      for (int i = 0; i <= 8; i++) {          // shift data in the rate array
        rate[i] = rate[i + 1];                // and drop the oldest IBI value
        runningTotal += rate[i];              // add up the 9 oldest IBI values
      }

      rate[9] = IBI;                          // add the latest IBI to the rate array
      runningTotal += rate[9];                // add the latest IBI to runningTotal
      runningTotal /= 10;                     // average the last 10 IBI values
      BPM = 60000 / runningTotal;             // how many beats can fit into a minute? that's BPM!
      QS = true;                              // set Quantified Self flag
      // QS FLAG IS NOT CLEARED INSIDE THIS ISR
    }
  }

  if (Signal < thresh && Pulse == true) {  // when the values are going down, the beat is over
    //    digitalWrite(blinkPin, LOW);           // turn off pin 13 LED
    Pulse = false;                         // reset the Pulse flag so we can do it again
    amp = P - T;                           // get amplitude of the pulse wave
    thresh = amp / 2 + T;                  // set thresh at 50% of the amplitude
    P = thresh;                            // reset these for next time
    T = thresh;
  }

  if (N > 2500) {                          // if 2.5 seconds go by without a beat
    thresh = 512;                          // set thresh default
    P = 512;                               // set P default
    T = 512;                               // set T default
    lastBeatTime = sampleCounter;          // bring the lastBeatTime up to date
    firstBeat = true;                      // set these to avoid noise
    secondBeat = false;                    // when we get the heartbeat back
  }

  sei();                                   // enable interrupts when youre done!
}// end isr
