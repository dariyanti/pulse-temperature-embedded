// Board yang digunakan Generic ESP8266 Module yang biasa
// Sketch yang dipakai menggunakan push button
// VCC => 3.3V
// CH_PD // CH_EN => 3.3V
// GND => GND
// UTXD => TX
// URXD => RX
// GPIO0 => GND
// GPI02 => VCC
// untuk RST dan button ini masukkan RST ke salah satu kaki kaki dari BUTTON,
// kemudian dari kaki kaki yang satunya sambungkan ke GND
// RST => BUTTON
// BUTTON => GND

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <OneWire.h>
#include <DallasTemperature.h>

ESP8266WiFiMulti WiFiMulti;

// silahkan diganti dulu sesuai dengan setting wifi dan lokal server
const char* ssid = "bersyukurlah";
const char* password = "dariyanti";
String host = "192.168.43.208"; // ip dari lokal server
uint16_t port = 80;

const int pulsePin = 0;
const int suhuPin = 2;
// setup sensor
OneWire oneWire(suhuPin);

// berikan nama variabel,masukkan ke pustaka Dallas
DallasTemperature sensorSuhu(&oneWire);

void setup() {
  pinMode(suhuPin, INPUT);
  Serial.begin(115200);
  // Serial.setDebugOutput(true);
  Serial.println();
  Serial.println();
  Serial.println();
  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }
  // mode station
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);

}

void loop() {
  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {
    // print aja semua info dari wifi yang terkoneksi
    Serial.print("[WIFI] connected...\n");
    Serial.print("[WIFI] IP :");
    Serial.printf("%s\n", WiFi.localIP().toString().c_str());
    Serial.print("[WIFI] GATEWAY :");
    Serial.printf("%s\n", WiFi.gatewayIP().toString().c_str());
    Serial.print("[WIFI] DNS :");
    Serial.printf("%s\n", WiFi.dnsIP().toString().c_str());

    WiFiClient client;
    HTTPClient http;
    Serial.print("[HTTP] begin...\n");

    // TODO GANTI RANDOM NUMBER INI DENGAN NILAI DARI DETAK JANTUNG
    int detakJantung = analogRead(pulsePin);
    if(detakJantung < 50){
      detakJantung +=55;
    }
    float suhu = ambilSuhu();

    Serial.print("[ALAT] Suhu Sekarang : ");
    Serial.print(suhu, 3);
    Serial.print("\n[ALAT] ♥");
    Serial.print(" BPM: ");
    Serial.print(detakJantung);
    Serial.println();

    // buat string url dengan method GET
    // kenapa gak diganti menjadi POST ? karena gak tau gimana mekanismenya, kwkwk

    String url = "/pulse-temperature-web/public/Test/postData?detak="; // alamat dari codeigniter yang dipake
    url += detakJantung;
    url += "&suhu=";
    url += suhu;

    // string url diatas akan menjadi semisal http://IP-DARI-LOKAL-SERVER:PORTNYA/pulse-temperature-web/public/Data/postData?detak=100&suhu=100

    if (http.begin(client, host, port, url)) {
      Serial.print("[HTTP] SEND ...\n");

      // kita cek dahulu, apakah kita mendapatkan respons dari server
      int httpCode = http.GET();

      // http code kalau negatif akan error
      // referensi dari ESP8266 error code https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266HTTPClient/src/ESP8266HTTPClient.h#L49

      if (httpCode > 0) {
        // oke kita dapat HTTP code nya
        Serial.printf("[HTTP] SEND ... code: %d\n", httpCode);

        // Response status code dari server
        // referensi dari HTTP Response status code https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
        // referensi dari ESP8266 status code https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266HTTPClient/src/ESP8266HTTPClient.h#L67

        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          // dapatkan payloadnya, kita print kan
          String payload = http.getString();
          Serial.println(payload);
        }

      } else {
        // kalau gak kekirim
        Serial.printf("[HTTP] SEND... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }
      http.end();
    } else {
      // kalao http nya gak konek, alias salah setting
      Serial.printf("[HTTP] Unable to connect\n");
    }
  } else {
    // kalau wifi gak konek
    Serial.print("[WIFI] not connected...\n");
  }
  // delay 1 detik
  delay(1000);
}

float ambilSuhu() {
  int val = map( analogRead(suhuPin), 0, 1023, 0, 110 );
  Serial.println(val);
  float mv = (val / 1024.0) * 5000;
  Serial.println(mv);
  float cel = mv * 10;
  return cel;
  //  sensorSuhu.requestTemperatures();
  //  float suhu = sensorSuhu.getTempCByIndex(0);
  //  return suhu;
}
