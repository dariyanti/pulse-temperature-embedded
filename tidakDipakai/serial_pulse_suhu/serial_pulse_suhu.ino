#define USE_ARDUINO_INTERRUPTS true
#include <PulseSensorPlayground.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SoftwareSerial.h> // memasukan library sofwareserial
SoftwareSerial wifi(3, 2); // RX, TX

#define WiFiSSID "vivo 1603"
#define WiFiPassword "rahasiadonk"
#define DestinationIP "192.168.43.208" // ip address 
//#define DestinationIP "http://sensor.lungguh.pw"
// sensor diletakkan di pin 2
//#define ONE_WIRE_BUS 2

// JANGAN DIUBAH
// ==============================================
volatile int rate[10];                    // array to hold last ten IBI values
volatile unsigned long sampleCounter = 0; // used to determine pulse timing
volatile unsigned long lastBeatTime = 0;  // used to find IBI
volatile int P = 512;                     // used to find peak in pulse wave, seeded
volatile int T = 512;                     // used to find trough in pulse wave, seeded
volatile int thresh = 525;                // used to find instant moment of heart beat, seeded
volatile int amp = 100;                   // used to hold amplitude of pulse waveform, seeded
volatile boolean firstBeat = true;        // used to seed rate array so we startup with reasonable BPM
volatile boolean secondBeat = false;      // used to seed rate array so we startup with reasonable BPM
// Volatile Variables, used in the interrupt service routine!
volatile int BPM;                   // int that holds raw Analog in 0. updated every 2mS
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // int that holds the time interval between beats! Must be seeded!
volatile boolean Pulse = false;     // "True" when User's live heartbeat is detected. "False" when not a "live beat".
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.
// ==============================================
// PIN SENSOR
int pulsePin = 2; // kabel serial dari pulse sensor
int suhuPin = 0;
// LED PIN
int blinkPin = 5; // LED untuk menggambarkan detak jantung

// setup sensor
//OneWire oneWire(ONE_WIRE_BUS);

// berikan nama variabel,masukkan ke pustaka Dallas
//DallasTemperature sensorSuhu(&oneWire);

float suhuSekarang;

boolean connected = false;

String suhuDikirim, detakDikirim;

int counter = 1;
void setup() {
  wifi.begin(115200);
  wifi.setTimeout(5000);
  Serial.begin(115200);
  Serial.println("ESP8266 Client Demo");
  delay(1000);
  // periksa apakah modul ESP8266 aktif
  wifi.println("AT+RST");
  delay(1000);
  if (wifi.find("OK")) {
    Serial.println("Modul siap");
  } else {
    Serial.println("Tidak ada respon dari modul");
    while (1);
  }
  delay(1000);
  //setelah modul siap, kita coba koneksi sebanyak 5 kali
  for (int i = 0; i < 5; i++) {
    connect_to_WiFi();
    if (connected) {
      break;
    }
  }
  if (!connected) {
    while (1);
  }
  delay(5000);
  // set the single connection mode
  wifi.println("AT+CIPMUX=0");
  delay(1000);
//  sensorSuhu.begin();
  interruptSetup(); // sets up to read Pulse Sensor signal every 2mS
}

void loop() {
    Serial.println(counter);
    if (QS == true) {    //  A Heartbeat Was Found
      // BPM and IBI have been Determined
      // Quantified Self "QS" true when arduino finds a heartbeat
      digitalWrite(blinkPin, HIGH);    // Blink LED, we got a beat.
      Serial.print("[BPM]: ");
      Serial.print(BPM);
      Serial.println();
      detakDikirim = String(BPM);
      digitalWrite(blinkPin, LOW);           // There is not beat, turn off pin 13 LED
      QS = false;                      // reset the Quantified Self flag for next time
    } else {
      Serial.println("[BPM]: ");
      Serial.print("0");
      Serial.println();
      detakDikirim = "0";
    }
    suhuSekarang = ambilSuhu();
//  BPM = random(80, 110);
  suhuSekarang = ambilSuhu();
  detakDikirim = String(BPM);
  Serial.print("[BPM]: ");
  Serial.print(BPM);
  Serial.println();
  Serial.print("[SUHU]: ");
  Serial.println(suhuSekarang);
  suhuDikirim = String(suhuSekarang);
  //  if (counter > 30) {
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += DestinationIP ;
  cmd += "\",80";
  wifi.println(cmd);
  Serial.println(cmd);
  if (wifi.find("Error")) {
    Serial.println("Koneksi error.");
//    counter = 0;
    return;
  }

  cmd = "GET /test/index.php?detak=";
  cmd += detakDikirim;
  //  cmd += "]";
  cmd += "&suhu=";
  cmd += suhuDikirim;
  //  cmd += "]";
  cmd += "\r\n"; // jangan lupa, setiap perintah selalu diakhiri dengan CR+LF
  Serial.print("[SEND]= ");
  Serial.print(cmd);
  Serial.print("AT+CIPSEND=");
  Serial.println(cmd.length());
  wifi.print("AT+CIPSEND=");
  wifi.println(cmd.length());
  if (wifi.find(">")) {
    Serial.print(">");
  } else {
    wifi.println("AT+CIPCLOSE");
    Serial.println("koneksi timeout");
    counter = 0;
    return;
  }
  wifi.print(cmd);
  //  delay(2000);
  while (wifi.available()) {
    char c = wifi.read();
    Serial.write(c);
    if (c == '\r') Serial.print('\n');
  }
  Serial.println("------end");
  //    counter = 0;
  //    suhuDikirim= "";
  //    detakDikirim="";
  //  }

  delay(1000);
//  counter ++;
}

void connect_to_WiFi() {
  wifi.println("AT+CWMODE=1");
  String cmd = "AT+CWJAP=\"";
  cmd += WiFiSSID;
  cmd += "\",\"";
  cmd += WiFiPassword;
  cmd += "\"";
  wifi.println(cmd);
  Serial.println(cmd);
  if (wifi.find("OK")) {
    Serial.println("Sukses, terkoneksi ke WiFi.");
    connected = true;
  } else {
    Serial.println("Tidak dapat terkoneksi ke WiFi. ");
    connected = false;
  }
}

float ambilSuhu()
{
    int val = analogRead(suhuPin);
    Serial.println(val);
    float mv = ( val / 1024.0) * 3300;
    Serial.println(mv);
    float cel = mv / 10;
    return cel;
//  sensorSuhu.requestTemperatures();
//  float suhu = sensorSuhu.getTempCByIndex(0);
//  return suhu;
}

void interruptSetup() {
  // Initializes Timer2 to throw an interrupt every 2mS.
  TCCR2A = 0x02;     // DISABLE PWM ON DIGITAL PINS 3 AND 11, AND GO INTO CTC MODE
  TCCR2B = 0x06;     // DON'T FORCE COMPARE, 256 PRESCALER
  OCR2A = 0X7C;      // SET THE TOP OF THE COUNT TO 124 FOR 500Hz SAMPLE RATE
  TIMSK2 = 0x02;     // ENABLE INTERRUPT ON MATCH BETWEEN TIMER2 AND OCR2A
  sei();             // MAKE SURE GLOBAL INTERRUPTS ARE ENABLED
}

// THIS IS THE TIMER 2 INTERRUPT SERVICE ROUTINE.
// Timer 2 makes sure that we take a reading every 2 miliseconds
ISR(TIMER2_COMPA_vect) {                        // triggered when Timer2 counts to 124
  cli();                                      // disable interrupts while we do this
  Signal = analogRead(pulsePin);              // read the Pulse Sensor
  //  Serial.println(Signal);
  //  delay(1000);
  sampleCounter += 2;                         // keep track of the time in mS with this variable
  int N = sampleCounter - lastBeatTime;       // monitor the time since the last beat to avoid noise

  //  find the peak and trough of the pulse wave
  if (Signal < thresh && N > (IBI / 5) * 3) { // avoid dichrotic noise by waiting 3/5 of last IBI
    if (Signal < T) {                       // T is the trough
      T = Signal;                         // keep track of lowest point in pulse wave
    }
  }

  if (Signal > thresh && Signal > P) {        // thresh condition helps avoid noise
    P = Signal;                             // P is the peak
  }                                        // keep track of highest point in pulse wave

  //  NOW IT'S TIME TO LOOK FOR THE HEART BEAT
  // signal surges up in value every time there is a pulse
  if (N > 250) {                                  // avoid high frequency noise
    if ( (Signal > thresh) && (Pulse == false) && (N > (IBI / 5) * 3) ) {
      Pulse = true;                               // set the Pulse flag when we think there is a pulse
      //      digitalWrite(blinkPin,HIGH);                // turn on pin 13 LED
      IBI = sampleCounter - lastBeatTime;         // measure time between beats in mS
      lastBeatTime = sampleCounter;               // keep track of time for next pulse

      if (secondBeat) {                      // if this is the second beat, if secondBeat == TRUE
        secondBeat = false;                  // clear secondBeat flag
        for (int i = 0; i <= 9; i++) {       // seed the running total to get a realisitic BPM at startup
          rate[i] = IBI;
        }
      }

      if (firstBeat) {                       // if it's the first time we found a beat, if firstBeat == TRUE
        firstBeat = false;                   // clear firstBeat flag
        secondBeat = true;                   // set the second beat flag
        sei();                               // enable interrupts again
        return;                              // IBI value is unreliable so discard it
      }


      // keep a running total of the last 10 IBI values
      word runningTotal = 0;                  // clear the runningTotal variable

      for (int i = 0; i <= 8; i++) {          // shift data in the rate array
        rate[i] = rate[i + 1];                // and drop the oldest IBI value
        runningTotal += rate[i];              // add up the 9 oldest IBI values
      }

      rate[9] = IBI;                          // add the latest IBI to the rate array
      runningTotal += rate[9];                // add the latest IBI to runningTotal
      runningTotal /= 10;                     // average the last 10 IBI values
      BPM = 60000 / runningTotal;             // how many beats can fit into a minute? that's BPM!
      QS = true;                              // set Quantified Self flag
      // QS FLAG IS NOT CLEARED INSIDE THIS ISR
    }
  }

  if (Signal < thresh && Pulse == true) {  // when the values are going down, the beat is over
    //    digitalWrite(blinkPin,LOW);            // turn off pin 13 LED
    Pulse = false;                         // reset the Pulse flag so we can do it again
    amp = P - T;                           // get amplitude of the pulse wave
    thresh = amp / 2 + T;                  // set thresh at 50% of the amplitude
    P = thresh;                            // reset these for next time
    T = thresh;
  }

  if (N > 2500) {                          // if 2.5 seconds go by without a beat
    thresh = 512;                          // set thresh default
    P = 512;                               // set P default
    T = 512;                               // set T default
    lastBeatTime = sampleCounter;          // bring the lastBeatTime up to date
    firstBeat = true;                      // set these to avoid noise
    secondBeat = false;                    // when we get the heartbeat back
  }

  sei();                                   // enable interrupts when youre done!
}// end isr
